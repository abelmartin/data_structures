# require '../../ruby/sorter.rb'
require File.expand_path("../../ruby/sorter", __FILE__)

RSpec.describe Sorter do
  shared_examples_for 'a functioning sort' do |sort_method|
    it 'sorts SET A correctly' do
      Sorter.send(sort_method, Sorter::SET_A).should == Sorter::SET_D
    end

    it 'sorts SET B correctly' do
      Sorter.send(sort_method, Sorter::SET_B).should == Sorter::SET_D
    end

    it 'sorts SET C correctly' do
      Sorter.send(sort_method, Sorter::SET_C).should == Sorter::SET_D
    end

    it 'sorts SET D correctly' do
      Sorter.send(sort_method, Sorter::SET_D).should == Sorter::SET_D
    end
  end

  describe 'constants' do
    it 'has expected sets' do
      Sorter::SET_A.should == [1,7,2,6,3,5,4]
      Sorter::SET_B.should == [4,2,3,7,6,5,1]
      Sorter::SET_C.should == [7,6,5,4,3,2,1]
      Sorter::SET_D.should == [1,2,3,4,5,6,7] #Sorted
    end
  end

  it_behaves_like 'a functioning sort', :bubblesort
  # it_behaves_like 'a functioning sort', :insertsort
  it_behaves_like 'a functioning sort', :quicksort
  it_behaves_like 'a functioning sort', :mergesort
  # it_behaves_like 'a functioning sort', :heapsort
end