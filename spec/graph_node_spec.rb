require File.expand_path("../../ruby/graph_node", __FILE__)
require 'pry-debugger'

RSpec.describe GraphNode do
  let(:node1){ GraphNode.new content: 1 }
  let(:node2){ GraphNode.new content: 2 }
  let(:node3){ GraphNode.new content: 3 }
  let(:node4){ GraphNode.new content: 4 }
  let(:node5){ GraphNode.new content: 5 }
  let(:node6){ GraphNode.new content: 6 }
  let(:node7){ GraphNode.new content: 7 }

  let(:all_nodes){ [node1, node2, node3, node4, node5, node6, node7] }
  let(:dijkstra_graph) do
    #http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
    node1.add_neighbor(node2, 7)
    node1.add_neighbor(node3, 9)
    node1.add_neighbor(node6, 14)

    node2.add_neighbor(node3, 10)
    node2.add_neighbor(node4, 15)

    node3.add_neighbor(node6, 2)
    node3.add_neighbor(node4, 11)

    node4.add_neighbor(node5, 6)

    node5.add_neighbor(node6, 9)
  end

  it 'responds to basic node properties' do
    %i( adjacent? neighbors content ).each do |prop|
      subject.should respond_to(prop)
    end
  end

  describe '#adjacent?' do
    it 'returns TRUE if node in neighbors set' do
      node1.instance_variable_set :@neighbors, [{node: node2, distance: 5}]

      node1.should be_adjacent(node2)
    end

    it 'returns FALSE if node is missing from neighbors set' do
      node1.should_not be_adjacent(node2)
    end
  end

  describe '#add_neighbor' do
    shared_examples_for 'reciprocal addition' do
      it "adds current node to the neighbor's set if node is missing" do
        node2.should_receive(:add_neighbor).and_call_original
        node1.add_neighbor(node2, 7)
      end

      it "does add current node to the neighbor's set if node is present" do
        node2.instance_variable_set :@neighbors, [{node: node1, distance: 7}]
        node2.should_not_receive(:add_neighbor)

        node1.add_neighbor(node2, 7)
      end
    end

    context 'when the neighbor is new' do
      it 'add the node as a neighor if the node is new' do
        node1.add_neighbor(node2, 7)

        node1.should be_adjacent(node2)
      end

      it_behaves_like 'reciprocal addition'
    end

    context 'when node is already a neighbor' do
      it 'does not add the node as a neighor' do
        node1.instance_variable_set :@neighbors, [{node: node2, distance: 7}]
        node1.add_neighbor(node2, 7)

        node1.neighbors.count{|neighbor| neighbor[:node] == node2}.should == 1
      end

      it_behaves_like 'reciprocal addition'
    end
  end

  describe '#shortest_path_dijkstra' do
    before{ dijkstra_graph }

    it 'returns empty array if destination is missing from graph' do
      disco_node = GraphNode.new(content: 99)

      node1.shortest_path_dijkstra(disco_node, all_nodes).should == nil
    end

    it 'returns array of nodes, ending with destination' do
      node1.shortest_path_dijkstra(node5, all_nodes).should == 20 #[node1,node3,node6,node5]
    end
  end
end