require File.expand_path("../../ruby/tree", __FILE__)

RSpec.describe Tree do
  let(:tree) do
  end

  let(:wiki_tree) do
    #http://upload.wikimedia.org/wikipedia/commons/f/f7/Binary_tree.svg
    node4 = Tree.new(content: 4)
    node9 = Tree.new(content: 9, children: [node4])
    node5 = Tree.new(content: 5, children: [node9])

    node6 = Tree.new(
      content: 6,
      children: [Tree.new(content: 5), Tree.new(content: 11)]
    )

    node7 = Tree.new(
      content: 7,
      children: [Tree.new(content: 2), node6]
    )

    Tree.new(content: 2, children: [node7, node5])
  end

  let(:alpha_tree) do
    node_d = Tree.new(content: 'd', children: [Tree.new(content: 'c'), Tree.new(content: 'e')])
    node_b = Tree.new(content: 'b', children: [Tree.new(content: 'a'), node_d])

    node_i = Tree.new(content: 'i', children: [Tree.new(content: 'h')])
    node_g = Tree.new(content: 'g', children: [nil, node_i])

    node_f = Tree.new(content: 'f', children: [node_b, node_g])
  end

  it 'responds to basic node properties' do
    %i( content parent children leaf?).each do |prop|
      subject.should respond_to(prop)
    end
  end

  describe 'traversal' do
    it 'performs pre-order walk' do
      alpha_tree.pre_order_array.should == %w(f b a d c e g i h)
      # wiki_tree.pre_order_array.should == [2,7,2,6,5,11,5,9,4]
    end

    it 'performs post-order walk' do
      alpha_tree.post_order_array.should == %w(a c e d b h i g f)
      # wiki_tree.post_order_array.should == [2,7,5,2,6,9,5,11,4]
    end

    it 'performs in-order walk' do
      alpha_tree.in_order_array.should == %w(a b c d e f g h i)
      # wiki_tree.in_order_array.should == [2,7,2,6,5,11,5,9,4]
    end

    it 'performs level-order walk' do
      alpha_tree.level_order_array.should == %w(f b g a d i c e h)
      # wiki_tree.level_order_array.should == [2,7,2,6,5,11,5,9,4]
    end
  end
end