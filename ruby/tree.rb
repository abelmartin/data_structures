class Tree
  def initialize(children: [], content: nil)
    @content = content
    @children = children
  end

  attr_accessor :parent, :children, :content

  def leaf?
    children.empty?
  end

  def pre_order_array
    pre_order_walk(self).flatten
  end

  def post_order_array
    post_order_walk(self).flatten
  end

  def in_order_array
    in_order_walk(self).flatten
  end

  def level_order_array
    content_array = []
    queue = [self]

    begin
      node = queue.delete_at(0)
      unless node.nil?
        content_array << node.content
        queue << node.children
        queue.flatten!
      end
    end while !queue.empty?

    content_array
  end

  private

  def pre_order_walk(node)
    return [] if node.nil?

    content_array = [node.content]

    node.children.each do |child|
      content_array << pre_order_walk(child)
    end

    content_array
  end

  def post_order_walk(node)
    content_array = []
    return content_array if node.nil?

    node.children.each do |child|
      content_array << post_order_walk(child)
    end

    content_array << node.content
  end

  def in_order_walk(node)
    content_array = []
    return content_array if node.nil?

    content_array << in_order_walk(node.children[0])
    content_array << node.content
    content_array << in_order_walk(node.children[1])
  end
end