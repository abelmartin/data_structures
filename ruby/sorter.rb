require 'set'

class Sorter
  SET_A = [1,7,2,6,3,5,4]
  SET_B = [4,2,3,7,6,5,1]
  SET_C = (1..7).to_a.reverse
  SET_D = (1..7).to_a

  #Be concerned about worst case space complexity
  #O(n^2)
  def self.bubblesort(set)
    #http://en.wikipedia.org/wiki/Bubble_sort
    set_array = Array.new(set)
    sorted = false

    begin
      sorted = true

      set_array.each_with_index do |current_item, idx|
        if (set_array.size - 1) > idx && current_item > set_array[idx+1]
          sorted = false
          swap(set_array, idx, idx+1)
        end
      end
    end while !sorted

    set_array
  end

  #How people sort a suit in a deck of cards
  #O(n^2)
  #FAIL :-(
  def self.insertsort(set)
    #http://en.wikipedia.org/wiki/Insertion_sort
    return # This is HELLA broken
    set_array = set.to_a
    pointer = 1

    # begin
    #   for idx in (pointer..1)
    #     if set_array[idx] <> set_array[idx-1] && idx != pointer

    #     ##delete_at(index)
    #     #val_to_move = set_array.delete_at(pointer)

    #     ##insert(index, value)
    #     #set_array.insert(idx, val_to_move)

    #     #Now that we've performed the insert, stop evaluations and move on
    #     break
    #   end

    #   pointer += 1
    # end while pointer < set_array.count
    for outer_idx in (1..set_array.size)
      pointer = outer_idx
      begin
        pointer = outer_idx
        swap(set_array, pointer, pointer-1)
        pointer -= 1
      end while pointer > 0 && set_array[pointer-1] > set_array[pointer]
    end

    set_array.to_set
  end

  #Split a list and sort elements around a pivot
  #Recursive
  #O(n log n)
  #WIN!
  def self.quicksort(set)
    # http://en.wikipedia.org/wiki/Quicksort
    set_array = Array.new(set)

    if set_array.size <= 1
      #return it
      set_array
    else
      #select pivot
      pivot = set_array.last

      #create before_pivot && after_pivot arrays
      before_pivot = []
      after_pivot = []

      #balance around pivot.
      #since it's the last element, we can skip it.
      #don't worry about dupes
      for idx in (0..set_array.size-2)
        value = set_array[idx]
        (value < pivot) ? before_pivot.push(value) : after_pivot.push(value)
      end

      #perform the recursive call and union their result, giving a new set
      quicksort(before_pivot) | [pivot] | quicksort(after_pivot)
    end
  end

  #Recursive
  #O(n log n)
  def self.quick_in_place(set, left, right)
    #Should use ::quick_ip_partition
  end

  def self.mergesort(set)
    set_array = Array.new(set)

    if set_array.size <= 1
      return set_array
    else
      split_idx = (set_array.size)/2
      set_a = mergesort( set_array[0...split_idx] )
      set_b = mergesort( set_array[(split_idx)...(set_array.size)] )
      merged_array = []

      idx_a, idx_b = 0, 0
      begin
        if idx_b == set_b.size || (idx_a != set_a.size && set_a[idx_a] < set_b[idx_b])
          merged_array << set_a[idx_a]
          idx_a += 1
        else
          merged_array << set_b[idx_b]
          idx_b += 1
        end
      end while idx_a != set_a.size || idx_b != set_b.size

      return merged_array
    end
  end

  def self.heap(set)
  end

  private

  def self.quick_ip_partition(array, left, right, pivot_index)
    #Implementaiton coming soon!
  end

  def self.swap(arr, index_a, index_b)
    temp_value = arr[index_a]
    arr[index_a] = arr[index_b]
    arr[index_b] = temp_value
  end

  def self.bubble_verbose(set)
    set_array = set.to_a
    ittr = 0

    sorted = false

    begin
      ittr += 1
      # puts "Itteration ##{ittr}"
      sorted = true

      set_array.each_with_index do |current_item, idx|
        # log(set_array, idx)
        next_item = set_array[idx+1]
        if (set_array.size - 1) > idx && current_item > next_item
          # puts "SWAP!"
          sorted = false
          set_array[idx+1] = current_item
          set_array[idx] = next_item
        end
      end
    end while !sorted

    puts set_array
    set_array.to_set
  end

  def self.log(arr, idx)
    puts "arr[#{idx}] = #{arr[idx]}"
    puts "arr[#{idx+1}] = #{arr[idx+1]}"
    puts "---"
  end
end
