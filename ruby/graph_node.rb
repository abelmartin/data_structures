class GraphNode
  def initialize(content: nil)
    @content = content
    @neighbors = [] #array of hashes with :node & :distance(vector) keys
  end

  attr_accessor :content
  attr_reader :neighbors

  def adjacent?(node)
    @neighbors.any?{ |neighbor| neighbor[:node] == node}
  end

  def add_neighbor(node, distance)
    # Check the current neighbors before adding the new node to them
    @neighbors << {node: node, distance: distance} unless adjacent?(node)

    # Add the current node to the incoming neighbor UNLESS it already exists
    node.add_neighbor(self, distance) unless node.adjacent?(self)
  end

  def shortest_path_dijkstra(destination, unvisited)
    current_node = self

    #Use node#content as key, and value is distance shortest distance.  Defaults to infinity-ish
    calculated_distances = {}
    unvisited.each{|uv| calculated_distances[uv.content] = 9999 }

    calculated_distances[self.content] = 0

    begin
      current_node.neighbors.each do |neighbor|
        temp_distance = calculated_distances[current_node.content] + neighbor[:distance]
        calculated_distances[neighbor[:node].content] = temp_distance if temp_distance < calculated_distances[neighbor[:node].content]
      end

      unvisited.delete current_node

      sorted_distances = calculated_distances.sort_by{ |k, v| v }
      next_node_content = sorted_distances.select{|k, _| unvisited.one?{|node| node.content == k} }.first[0]
      current_node = unvisited.select{|uv| uv.content == next_node_content}.first

    end while unvisited.any? && unvisited.one?{|node| node == destination}

    calculated_distances[destination.content]
  end


  def to_s
    neighborhood = @neighbors.map.with_index do |n, i|
      "##{i} => Content: #{n[:node].content} / Distance: #{n[:distance]}"
    end.join(' | ')

    neighborhood = neighborhood.empty? ? nil : "{ #{neighborhood} }"

    "Content: #{@content}, Neighbors: #{neighborhood || 'Alone'}"
  end
end